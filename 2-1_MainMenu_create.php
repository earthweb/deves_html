<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>เข้าสู่ระบบ</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มเมนู</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>เมนู</span></li>
                            <li><span>เพิ่มเมนู</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">เพิ่มเมนู</h2>
                                </header>
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อเมนู <span class="required">*</span></label>
                                        <div class="col-lg-6">
                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="30" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2 col-lg-3">สถานะแสดงผล</label>
                                        <div class="col-lg-9">
                                            <div class="switch switch-success">
                                                <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                            <button type="reset" class="btn btn-default">รีเซ็ต</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>




            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>