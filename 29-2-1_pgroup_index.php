<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบกำหนดสิทธิการใช้งาน</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>กลุ่มผู้ใช้งาน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบกำหนดสิทธิการใช้งาน</span></li>
                            <li><span>กลุ่มผู้ใช้งาน</span></li>
                            <li><span>create</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>




                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">จัดการ group</h2>
                                </header>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                                Fields with * are required.
                                                <div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">Group Name <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>

                                                    <div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-3 control-label text-sm-right pt-2">control_group</label>
                                                            <div class="col-lg-6">
                                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            </div>
                                                        </div>


                                                        <div class="form-group row">
                                                            Permission
                                                            <label class="col-sm-3 control-label text-sm-right pt-2">Controller</label>
                                                            <div class="col-lg-6">
                                                                ข้อมูลสมาชิก
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-3 control-label text-sm-right pt-2">Action</label>
                                                            <div class="col-lg-6">
                                                                <label class="checkbox-inline">
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> แก้ไข
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> ลบข้อมูล
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> เพิ่มข้อมูล
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> ตั้งค่าการลงทะเบียน
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> เพิ่มสมาชิกจาก Excle
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> รายงานการสมัครสมาชิก
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> รีเซ็ตรหัสผ่าน
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-3 control-label text-lg-right pt-2 col-lg-3">Active</label>
                                                            <div class="col-lg-9">
                                                                <div class="switch switch-success">
                                                                    <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">

                                                            <label class="col-sm-3 control-label text-sm-right pt-2">Controller</label>
                                                            <div class="col-lg-6">
                                                                ข้อความส่วนตัว
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-sm-3 control-label text-sm-right pt-2">Action</label>
                                                            <div class="col-lg-6">
                                                                <label class="checkbox-inline">
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> แก้ไข
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> ลบข้อมูล
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> เพิ่มข้อมูล
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> ตั้งค่าการลงทะเบียน
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> เพิ่มสมาชิกจาก Excle
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> รายงานการสมัครสมาชิก
                                                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> รีเซ็ตรหัสผ่าน
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-lg-3 control-label text-lg-right pt-2 col-lg-3">Active</label>
                                                            <div class="col-lg-9">
                                                                <div class="switch switch-success">
                                                                    <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> create</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>