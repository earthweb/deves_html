<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบชุดข้อสอบ</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มชุดข้อสอบอบรมออนไลน์</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบชุดข้อสอบ</span></li>
                            <li><span>เพิ่มชุดข้อสอบอบรมออนไลน์</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-edit"></i> Import excel ข้อสอบ</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <section class="card card-featured-primary mb-4">
                                            <div class="card-body shadow-none">
                                                <div class="widget-summary">
                                                    <div class="widget-summary-col widget-summary-col-icon">
                                                        <div class="summary-icon bg-primary">
                                                            <i class="fas fa-download"></i>
                                                        </div>
                                                    </div>
                                                    <div class="widget-summary-col">
                                                        <div class="summary">
                                                            <h4 class="title">แบบฟอร์มรูปแบบนำเข้าแบบทดสอบ</h4>
                                                            <div class="info">
                                                                <a href=""><span class="text-primary">Download Excel</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                    <div class="col-lg-6">
                                        <section class="card card-featured-primary mb-4 ">
                                            <div class="card-body shadow-none">
                                                <div class="widget-summary">
                                                    <div class="widget-summary-col widget-summary-col-icon">
                                                        <div class="summary-icon bg-primary">
                                                            <i class="fas fa-file-excel"></i>
                                                        </div>
                                                    </div>
                                                    <div class="widget-summary-col">
                                                        <div class="summary">
                                                            <h4 class="title">ไฟล์ Excel Import</h4>
                                                            <div class="info">
                                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <div class="uneditable-input">
                                                                            <i class="fas fa-file fileupload-exists"></i>
                                                                            <span class="fileupload-preview"></span>
                                                                        </div>
                                                                        <span class="btn btn-default btn-file">
                                                                            <span class="fileupload-exists">Change</span>
                                                                            <span class="fileupload-new">Select file</span>
                                                                            <input type="file" />
                                                                        </span>
                                                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                    </div>
                                                                </div>
                                                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                
                                <table class="table table-responsive-md mb-0">
											<thead>
												<tr>
													<th>ประเภทคำตอบ</th>
													<th>ความหมาย</th>
												
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>radio</td>
													<td>ข้อสอบคำตอบเดียว</td>
												</tr>
                                                <tr>
													<td>checkbox</td>
													<td>ข้อสอบหลายคำตอบ</td>
												</tr>
                                                <tr>
													<td>textarea</td>
													<td>ข้อสอบบรรยาย</td>
												</tr>
                                                <tr>
													<td>dropdown</td>
													<td>ข้อสอบจับคู่</td>
												</tr>
                                                <tr>
													<td>hidden</td>
													<td>จัดเรียง</td>
												</tr>
											</tbody>
										</table>
                                       <p> การใส่สัญลักษณ์ <span class="text-danger">*หน้าตัวเลือก</span> หมายความว่า ตัวเลือกนั้นคือคำตอบที่ถูกต้อง</p>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>