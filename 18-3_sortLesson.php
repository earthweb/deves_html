<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>หลักสูตร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดเรียงบทเรียน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>หลักสูตร</span></li>
                            <li><span>จัดเรียงบทเรียน</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title">บทเรียนทั้งหมด</h2>
                            </header>
                            <div class="card-body">
                                <div class="section-sort">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="dd" id="nestable">
                                                <ol class="dd-list">
                                                    <li class="dd-item" data-id="1">
                                                        <div class="dd-handle">Item 1</div>
                                                    </li>
                                                    <li class="dd-item" data-id="2">
                                                        <div class="dd-handle">Item 2</div>
                                                    </li>
                                                    <li class="dd-item" data-id="3">
                                                        <div class="dd-handle">Item 3</div>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-lg-6">
                                            <label for="">start debug ลบออกได้เลยครับ</label>
                                            <textarea id="nestable-output" rows="3" class="form-control"></textarea>
                                        </div>
                                        <!-- end debug -->
                                    </div>
                                </div>
                                <hr>
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
    <script src="vendor/jquery-nestable/jquery.nestable.js"></script>
    <script>
        (function($) {

            'use strict';

            /*
            Update Output
            */
            var updateOutput = function(e) {
                var list = e.length ? e : $(e.target),
                    output = list.data('output');

                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize')));
                } else {
                    output.val('JSON browser support required for this demo.');
                }
            };

            /*
            Nestable 1
            */
            $('#nestable').nestable({
                group: 1
            }).on('change', updateOutput);

            /*
            Output Initial Serialised Data
            */
            $(function() {
                updateOutput($('#nestable').data('output', $('#nestable-output')));
            });

        }).apply(this, [jQuery]);
    </script>
</body>

</html>