<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบcaptcha</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มcaptcha</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบcaptcha</span></li>
                            <li><span>เพิ่มcaptcha</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>




                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">เพิ่มcaptcha</h2>
                                </header>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                                <div>
                                                    <div class="alert alert-danger mt-2">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        ค่าที่มี <i class="fas fa-question-circle"></i> จำเป็นต้องใส่ให้ครบ
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อเงื่อนไข <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            <div class="alert alert-danger mt-2">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                                <i class="fas fa-question-circle"></i> ชื่อเงื่อนไข ไม่ควรเป็นค่าว่าง
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">หลักสูตร <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup label="">
                                                                    <option value="AK">บทที่1</option>
                                                                    <option value="HI">บทที่2</option>
                                                                </optgroup>
                                                            </select>
                                                        </div><i class="fas fa-question-circle"></i>
                                                    </div>

                                                    
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ระยะเวลาการแสดงแคปช่า (ทุกๆนาที) <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            <div class="alert alert-danger mt-2">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                                <i class="fas fa-question-circle"></i> ระยะเวลาการแสดงแคปช่า (ทุกๆนาที) ไม่ควรเป็นค่าว่าง
                                                            </div>
                                                        </div>Note: หน่วยเป็นนาที
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">กำหนดการเรียนย้อนหลัง <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            <div class="alert alert-danger mt-2">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                                <i class="fas fa-question-circle"></i> กำหนดการเรียนย้อนหลัง ไม่ควรเป็นค่าว่าง
                                                            </div>
                                                        </div>Note: หน่วยเป็นนาที
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ระยะเวลาการตอบ <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            <div class="alert alert-danger mt-2">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                                <i class="fas fa-question-circle"></i> ระยะเวลาการตอบ ไม่ควรเป็นค่าว่าง
                                                            </div>
                                                        </div>Note: หน่วยเป็นวินาที
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">จำนวนครั้งที่ตอบผิด <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            <div class="alert alert-danger mt-2">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                                <i class="fas fa-question-circle"></i> จำนวนครั้งที่ตอบผิด ไม่ควรเป็นค่าว่าง
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2 col-lg-3">แสดงผล</label>
                                        <div class="col-lg-9">
                                            <div class="switch switch-success">
                                                <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                            </div>
                                        </div>
                                    </div>
                                                    

                                                </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>