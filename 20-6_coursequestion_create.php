<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบชุดข้อสอบ</title>
    <?php include 'include/inc-head.php'; ?>

    <style>
        .radio input[type="radio"],
        .checkbox input[type="checkbox"] {
            height: 40px;
            width: 30px;
        }
    </style>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มข้อสอบชุดข้อสอบ </h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบชุดข้อสอบ</span></li>
                            <li><span>เพิ่มข้อสอบชุดข้อสอบ </span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-edit"></i> เพิ่มข้อสอบ</h2>
                            </header>
                            <div class="card-body">

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <button type="button" id="btnadd-exam-1" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มข้อสอบคำตอบเดียว</button>
                                        <button type="button" id="btnadd-exam-2" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มข้อสอบหลายคำตอบ</button>
                                        <button type="button" id="btnadd-exam-3" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มข้อสอบบรรยาย</button>
                                        <button type="button" id="btnadd-exam-4" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มข้อสอบจับคู่</button>
                                        <button type="button" id="btnadd-exam-5" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มข้อสอบจัดเรียง</button>
                                        <h4 class="float-right"><strong>จำนวนข้อที่สร้าง <span class="text-info">1</span> ข้อ</strong></h4>
                                    </div>
                                </div>

                                <div class="mt-5 pt-3 border-top" style="display:none" id="exam-1">
                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">โจทย์ (คำตอบเดียว) <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบโจทย์</button></label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">อธิบายคำตอบ</label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">ตัวเลือก <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มตัวเลือก</button> </label>
                                        <div class="col-lg-9">

                                            <div class="form-group row">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                                                    </label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" checked="">
                                                    </label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="mt-5 pt-3 border-top" style="display:none" id="exam-2">
                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">โจทย์ (หลายคำตอบ) <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบโจทย์</button></label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">อธิบายคำตอบ</label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">ตัวเลือก <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มตัวเลือก</button> </label>
                                        <div class="col-lg-9">

                                            <div class="form-group row">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                    </label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="">
                                                    </label>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="mt-5 pt-3 border-top" style="display:none" id="exam-3">
                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">โจทย์ (คำตอบบรรยาย) <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบโจทย์</button></label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-5 pt-3 border-top" style="display:none" id="exam-4">
                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">โจทย์ (จับคู่) <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบโจทย์</button></label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">อธิบายคำตอบ</label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">เพิ่มส่วนที่ 1 เพิ่มคำถามและคำตอบ <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มตัวเลือก</button> </label>
                                        <div class="col-lg-9">
                                            <div class="form-group row">
                                                <label class="pt-2">คำถาม</label>
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบตัวเลือก</button>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="pt-2">คำตอบ</label>
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบตัวเลือก</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">เพิ่มส่วนที่ 2 เพิ่มคำตอบหลอก <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มตัวเลือก</button> </label>
                                        <div class="col-lg-9">
                                            <div class="form-group row">
                                                <label class="pt-2">คำตอบ</label>
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบตัวเลือก</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-5 pt-3 border-top" style="display:none" id="exam-5">
                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">โจทย์ (จัดเรียง) <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-danger"><i class="fas fa-times"></i> ลบโจทย์</button></label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">อธิบายคำตอบ</label>
                                        <div class="col-lg-9">
                                            <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">ตัวเลือก <br><button type="button" class="mb-1 mt-1 mr-1 btn btn-success"><i class="far fa-file-alt"></i> เพิ่มตัวเลือก</button> </label>
                                        <div class="col-lg-9">

                                            <div class="form-group row">
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-9">
                                                    <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
    <script>
        (function($) {
            $("#btnadd-exam-1").click(function() {
                $("#exam-1").css("display","block");
            });
            $("#btnadd-exam-2").click(function() {
                $("#exam-2").css("display","block");
            });
            $("#btnadd-exam-3").click(function() {
                $("#exam-3").css("display","block");
            });
            $("#btnadd-exam-4").click(function() {
                $("#exam-4").css("display","block");
            });
            $("#btnadd-exam-5").click(function() {
                $("#exam-5").css("display","block");
            });
            // $(".summernote").summernote({
            //     height: 180,
            //     toolbar: [
            //         ['style', ['style']],
            //         ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            //         ['fontname', ['fontname']],
            //         ['fontsize', ['fontsize']],
            //         ['color', ['color']],
            //         ['para', ['ol', 'ul', 'paragraph', 'height']],
            //         ['table', ['table']],
            //         ['insert', ['link']],
            //         ['view', ['undo', 'redo', 'fullscreen', 'codeview', 'help']]
            //     ]
            // });
        }).apply(this, [jQuery]);
    </script>

</body>

</html>