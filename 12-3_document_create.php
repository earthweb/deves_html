<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>เอกสาร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มเอกสาร (ภาษา EN)</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>เอกสาร</span></li>
                            <li><span>เพิ่มเอกสาร (ภาษา EN)</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>




                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">เพิ่มหมวดเอกสาร</h2>
                                </header>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                                <div>
                                                    <div class="alert alert-danger mt-2">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        ค่าที่มี <i class="fas fa-question-circle"></i> จำเป็นต้องใส่ให้ครบ
                                                    </div>



                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">รหัสประเภทของไฟล์ (ภาษา EN )</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup>
                                                                    <option value="HI">--------select DocumentType---------</option>
                                                                    <option value="HI">select DocumentType</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">สถานะสิทธิ์การเข้าถึง (ภาษา EN ) </label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup>
                                                                    <option value="HI">--------select DocumentType---------</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อของไฟล์ดาวน์โหลด (ภาษา EN ) <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup>
                                                                    <option value="HI">--------select DocumentType---------</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <form class="form-horizontal form-bordered">
                                                        <div class="form-group row">
                                                            <label class="col-lg-3 control-label text-lg-right pt-2">รายละเอียดไฟล์ดาวน์โหลด (ภาษา EN )</label>
                                                            <div class="col-lg-9">
                                                                <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>


                                                            </div>
                                                    </form>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-3 control-label text-lg-right pt-2">ตำแหน่งที่ตั้งไฟล์ (ภาษา EN )</label>
                                                    <div class="col-lg-6">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="input-append">
                                                                <div class="uneditable-input">
                                                                    <i class="fas fa-file fileupload-exists"></i>
                                                                    <span class="fileupload-preview"></span>
                                                                </div>
                                                                <span class="btn btn-default btn-file">
                                                                    <span class="fileupload-exists">เปลืยน</span>
                                                                    <span class="fileupload-new">เลือกไฟล์</span>
                                                                    <input type="file" />
                                                                </span>
                                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">ลบ</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>