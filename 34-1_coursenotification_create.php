<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ตั้งค่าการแจ้งเตือนบทเรียน</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>สร้างระบบแจ้งเตือนบทเรียน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ตั้งค่าการแจ้งเตือนบทเรียน</span></li>
                            <li><span>สร้างระบบแจ้งเตือนบทเรียน</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">สร้างระบบแจ้งเตือนบทเรียน</h2>
                                </header>

                                <div class="card-body">
                                                <div>
                                                    <div class="alert alert-danger mt-2">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        ค่าที่มี <i class="fas fa-question-circle"></i> จำเป็นต้องใส่ให้ครบ
                                                    </div>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                                <div>

                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2"></label>
                                                        <div class="col-lg-6">
                                                            <select multiple="" class="form-control">
                                                                <option>      
                                                                 1
                                                                </option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                                <option>5</option>
                                                                <option>5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">รุ่น<span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup label="">
                                                                    <option value="AK">---กรุณาเลือกรุ่น---</option>
                                                                    <option value="HI">1</option>
                                                                    <option value="HI">2</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ระยะเวลาแจ้งเตือนก่อนสิ้นสุด (วัน)<span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup label="">
                                                                    <option value="AK">---กรุณาเลือกวัน---</option>
                                                                    <option value="HI">1</option>
                                                                    <option value="HI">2</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">สถานะ</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup label="">
                                                                    <option value="AK">เปิดใช้งาน</option>
                                                                    <option value="HI">ปิดใช้งาน</option>
                                                                    
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                        <button class="btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>


            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>