<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบข่าวประชาสัมพันธ์</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการข่าวประชาสัมพันธ์</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            
                            <li><span>ระบบข่าวประชาสัมพันธ์</span></li>
                            <li><span>จัดการข่าวประชาสัมพันธ์</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-search"></i> ค้นหาขั้นสูง</h2>
                            </header>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อหัวข้อ (ภาษา EN )</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                        <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-search"></i> ค้นหา</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-table"></i> จัดการข่าวประชาสัมพันธ์</h2>
                            </header>
                            <div class="card-body">
                                <table class="table table-bordered  mb-0" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th class="">รูปภาพ</th>
                                            <th class="text-center" class="" width="300px">ชื่อหัวข้อ (ภาษา EN )</th>
                                            <th class="text-center" width="300px">วันที่แก้ไขข้อมูล (ภาษา EN )</th>
                                            <th class="text-center" width="90px">ย้าย</th>
                                            <th class="text-center" width="90px">ภาษา</th>
                                            <th class="text-center" width="90px">จัดการ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <div class="btn-group-vertical d-block">
                                                
                                                </div>
                                            </td>
                                            <td class="actions text-center">
                                            
                                            </td>
                                            <td class="actions text-center">
                                               
                                            </td>
                                            <td class="actions text-center">
                                               
                                            </td>
                                            <td class="actions text-center">
                                                
                                            </td>
                                            
                                        </tr>
                                       
                                    
                                    </tbody>
                                </table>

                                <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-trash"></i> ลบข้อมูลทั้งหมด</button>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>