<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
            data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main">
                    <li class="nav-active">
                        <a class="nav-link" href="index.php">
                            <i class="bx bx-home-alt"></i>
                            <span>หน้าหลัก</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-flag'></i>
                            <span>ภาษา</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="1-1_language_create.php">
                                    เพิ่มภาษา
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="1-2_language.php">
                                    จัดการภาษา
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-grid'></i>
                            <span>เมนู</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="2-1_MainMenu_create.php">
                                    เพิ่มเมนู
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="2-2_MainMenu_admin.php">
                                    จัดการเมนู
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="">
                        <a class="nav-link" href="3_usage_statistics.php">
                            <i class='bx bxs-brightness'></i> <span>สถิติการใช้งาน</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="nav-link" href="4_basic_system_setup.php">
                            <i class='bx bxs-brightness'></i> <span>ตั้งค่าระบบพื้นฐาน</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-news'></i>
                            <span>เงื่อนไขการใช้งานระบบ</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="5-1_conditions_system.php">
                                    แก้ไขเงือนไขการใช้งาน
                                </a>
                            </li>
                           
                        </ul>
                    </li>
                    <li class="">
                        <a class="nav-link" href="6_pdpa_conditions.php">
                            <i class='bx bxs-brightness'></i> <span>เงื่อนไขPDPA</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="nav-link" href="7_contactus_system.php">
                            <i class='bx bxs-brightness'></i> <span>ระบบติดต่อเรา</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-news'></i>
                            <span>ป้ายประชาสัมพันธ์</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="8-1_imgslide_create.php">
                                    เพิ่มป้ายประชาสัมพันธ์
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="8-2_imgslide_index.php">
                                    จัดการป้ายประชาสัมพันธ์
                                </a>
                            </li>

                        </ul>
                    </li>
                     <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-news'></i>
                            <span>ระบบข่าวประชาสัมพันธ์</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="9-1_news_create.php">
                                    เพิ่มข่าวประชาสัมพันธ์
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="9-2_news_index.php">
                                    จัดการข่าวประชาสัมพันธ์
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-news'></i>
                            <span>ระบบวิดีโอแนะนำ</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="10-1_add_video.php">
                                    เพิ่มวิดีโอแนะนำ
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="10-2manage_videos.php">
                                    จัดการวิดีโอแนะนำ
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-news'></i>
                            <span>ระบบจัดการป๊อปอัพ</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="11-1_popup_create.php">
                                    เพิ่มข่าวป๊อปอัพ
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="11-2_popup_admin.php">
                                    จัดการป๊อปอัพ
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-news'></i>
                            <span>ระบบเอกสาร</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="12-1_document_createtype.php">
                                    เพิ่มประเภทเอกสาร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="12-2_document_Index_type.php">
                                    จัดการประเภทเอกสาร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="12-3_document_create.php">
                                    เพิ่มเอกสาร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="12-4_document_index.php">
                                    จัดการเอกสาร
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-folder'></i>
                            <span>ระบบวิธีการใช้งาน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="13-1_usability_create.php">
                                    เพิ่มวิธีการใช้งาน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="13-2_usability_index.php">
                                    จัดการวิธีการใช้งาน
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-help-circle'></i>
                            <span>คำถามที่พบบ่อย</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="14-1_faqtype_index.php">
                                    หมวดคำถาม
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="14-2_faq_index.php">
                                    คำถามที่พบบ่อย
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-error-circle'></i>
                            <span>ประเภทปัญหาการใช้งาน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="15-1_problem_type_create.php">
                                    เพิ่มประเภทปัญหาการใช้งาน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="15-2_problem_type_index.php">
                                    จัดการประเภทปัญหาการใช้งาน
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="">
                        <a class="nav-link" href="16_operation_problem_system.php">
                            <i class='bx bxs-error-circle'></i>
                            <span>ระบบปัญหาการใช้งาน</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-desktop'></i>
                            <span class="badge badge-primary">1</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            <span>หมวดหลักสูตร</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="17-1_category_create.php">
                                    เพิ่มหมวดหลักสูตร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="17-2_category_index.php">
                                    จัดการหมวดหลักสูตร
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-desktop'></i>
                            <span class="badge badge-primary">2</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            <span>หลักสูตร</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="18-1_courseOnline_create.php">
                                    เพิ่มหลักสูตร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="18-2_courseOnline_index.php">
                                    จัดการหลักสูตร
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-desktop'></i>
                            <span class="badge badge-primary">3</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            <span>ระบบบทเรียน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="19-1_lesson_create.php">
                                    เพิ่มบทเรียน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="19-2_lesson_index.php">
                                    จัดการบทเรียน
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-folder-open'></i>
                            <span class="badge badge-primary">4</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            <span>ระบบชุดข้อสอบ</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="20-1_grouptesting_create.php">
                                    เพิ่มชุดข้อสอบบทเรียน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="20-2_grouptesting_index.php">
                                    จัดการชุดข้อสอบบทเรียน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="20-3_coursegrouptesting_create.php">
                                    เพิ่มชุดข้อสอบหลักสูตร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="20-4_coursegrouptesting_index.php">
                                    จัดการชุดข้อสอบหลักสูตร
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-report'></i>
                            <span class="badge badge-primary">5</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            <span>ระบบแบบสอบถาม</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="21-1_questionnaire_create.php">
                                    เพิ่มแบบสอบถาม
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="21-2_questionnaire_index.php">
                                    จัดการแบบสอบถาม
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-report'></i>
                            <span>ระบบจัดการชั้นเรียน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="22-1_manage_organization_chart.php">
                                    จัดการ Organization Chart
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-printer'></i>
                            <span>ระบบใบประกาศนียบัตร</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="23-1_certificate_index.php">
                                    จัดการประกาศนียบัตร
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="23-2_signature_index.php">
                                    จัดการลายเซนต์
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-report'></i>
                            <span>ระบบตรวจข้อสอบบรรยาย</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="24-1_check_lecture.php">
                                    ตรวจข้อสอบบรรยายบทเรียน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="24-2_check_course_lecture.php">
                                    ตรวจข้อสอบบรรยายหลักสูตร
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-report'></i>
                            <span>ระบบแผนการเรียน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="25-1_manage_study_plans.php">
                                    จัดการแผนการเรียน
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-report'></i>
                            <span>ห้องเรียนออนไลน์</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="26-1_add_online_class.php">
                                    เพิ่มห้องเรียนออนไลน์
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="26-2manage_online_classroom.php">
                                    จัดการห้องเรียนออนไลน์
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="">
                        <a class="nav-link" href="27_Examination_results_reset_system.php">
                            <i class='bx bxs-report'></i>
                            <span>ระบบรีเซ็ทผลการเรียนการสอบ</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-brightness'></i>
                            <span>ระบบข้อมูลสมาชิก</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="28-1_add_member_information.php">
                                    เพิ่มข้อมูลสมาชิก
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="28-2_add_member_from_Excel.php">
                                    เพิ่มข้อมูลสมาชิกจาก Excel
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="28-3_List_staff_members.php">
                                    รายชื่อสมาชิกพนักงาน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="28-4_guest_members_list.php">
                                    เพิ่มข้อมูลสมาชิกบุคคลทั่วไป
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-folder-open'></i>
                            <span>ระบบการกำหนดสิทธิการใช้งาน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="29-1_adminuser_index.php">
                                    ข้อมูลผู้ดูแลระบบ
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="29-2_pgroup_index.php">
                                    กลุ่มผู้ใช้งาน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="29-3_pcontroller_index.php">
                                    เพิ่ม Controller
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-folder-open'></i>
                            <span>ห้องสมุดออนไลน์ (E-Library)</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="30-1_manage_library_types.php">
                                    จัดการประเภทห้องสมุด
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="30-2_manage_Library_E_Library.php">
                                    จัดการห้องสมุด(E-Library)
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="30-3_manage_download_approvals.php">
                                    เพิ่ม Controller
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bx-folder-open'></i>
                            <span>ระบบตั้งค่าตรวจสอบใบหน้าหลักสูตร</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="31_manage_face_settings.php">
                                    จัดการตั้งค่าตรวจสอบใบหน้าหลักสูตร
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-printer'></i>
                            <span>ระบบ Captcha</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="32-1_captcha_index.php">
                                    ตั้งค่า Captcha
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-printer'></i>
                            <span>ระบบพิมพ์ใบประกาศนียบัตร</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="33-1_passcours_index.php">
                                    รายงานผู้ผ่านการเรียน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="33-2_passcours_passcourslog.php">
                                    รายงานสถิติจำนวนผู้พิมพ์ใบประกาศนียบัตร
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-printer'></i>
                            <span>ระบบตั้งค่าการแจ้งเตือนบทเรียน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="34-1_coursenotification_create.php">
                                    สร้างระบบแจ้งเตือนบทเรียน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="34-2_coursenotification_index.php">
                                    จัดการระบบแจ้งเตือนบทเรียน
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="">
                        <a class="nav-link" href="35_student_tracking_system.php">
                            <i class='bx bxs-printer'></i>
                            <span>ระบบติดตามผู้เรียน</span>
                        </a>
                       
                    </li>
                    <li class="">
                        <a class="nav-link" href="36_report_system.php">
                            <i class='bx bxs-printer'></i>
                            <span>ระบบ Report</span>
                        </a>
                       
                    </li>
                    <li class="">
                        <a class="nav-link" href="37_system for submitting.php">
                            <i class='bx bxs-printer'></i>
                            <span>ระบบการส่งผลการเรียนผ่านทางระบบอัตโนมัติ</span>
                        </a>
                       
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class='bx bxs-error-circle'></i>
                            <span>ระบบเก็บ Log การใช้งาน</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="38-1_logadmin_user.php">
                                    Log การใช้งานผู้เรียน
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="38-2_log_admin_index.php">
                                    Log การใช้งานผู้ดูแลระบบ
                                </a>
                            </li>

                        </ul>
                    </li>
                    
                    

                </ul>
            </nav>

            <hr class="separator" />

            <!-- <div class="sidebar-widget widget-tasks">
                <div class="widget-header">
                    <h6>Projects</h6>
                    <div class="widget-toggle">+</div>
                </div>
                <div class="widget-content">
                    <ul class="list-unstyled m-0">
                        <li><a href="#">Porto HTML5 Template</a></li>
                        <li><a href="#">Tucson Template</a></li>
                        <li><a href="#">Porto Admin</a></li>
                    </ul>
                </div>
            </div> -->

            <hr class="separator" />

            <!-- <div class="sidebar-widget widget-stats">
                <div class="widget-header">
                    <h6>Company Stats</h6>
                    <div class="widget-toggle">+</div>
                </div>
                <div class="widget-content">
                    <ul>
                        <li>
                            <span class="stats-title">Stat 1</span>
                            <span class="stats-complete">85%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary progress-without-number"
                                    role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 85%;">
                                    <span class="sr-only">85% Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span class="stats-title">Stat 2</span>
                            <span class="stats-complete">70%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary progress-without-number"
                                    role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 70%;">
                                    <span class="sr-only">70% Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <span class="stats-title">Stat 3</span>
                            <span class="stats-complete">2%</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary progress-without-number"
                                    role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 2%;">
                                    <span class="sr-only">2% Complete</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div> -->
        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>


    </div>

</aside>