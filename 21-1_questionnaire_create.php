<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบแบบสอบถาม</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มแบบสอบถาม</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบแบบสอบถาม</span></li>
                            <li><span>เพิ่มแบบสอบถาม</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>




                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">เพิ่มแบบสอบถาม</h2>
                                </header>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                                <div>
                                                    <div class="alert alert-danger mt-2">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        ค่าที่มี <i class="fas fa-question-circle"></i> จำเป็นต้องใส่ให้ครบ
                                                    </div>




                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">หัวข้อแบบสอบถาม <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>




                                                    <form class="form-horizontal form-bordered">
                                                        <div class="form-group row">
                                                            <label class="col-lg-3 control-label text-lg-right pt-2">รายละเอียดแบบสอบถาม</label>
                                                            <div class="col-lg-9">
                                                                <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>


                                                            </div>
                                                        </div>

                                                    </form>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2"><input type="checkbox" checked="" id="checkboxExample2"> สถานะบังคับกรอก <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2"><button type="button" class="mb-1 mt-1 mr-1 btn btn-success" href="20-4_coursegrouptesting_index.php">เพิ่มกลุ่ม</button></label>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2"> ชื่อกลุ่ม </label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-success" >เพิ่มคำถามแบบตอบบรรทัดเดียว</button>
                                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-success" >เพิ่มคำถามแบบคำตอบเดียว</button>
                                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-success" >เพิ่มคำตอบแบบหลายคำตอบ</button>
                                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-success" >เพิ่มคำถามแบบให้คะแนน</button>
                                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-success" >เพิ่มคำถามบรรยาย</button>
                                                            <button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">คำถาม (แบบตอบบรรทัดเดียว)</label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div><button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">คำถาม (แบบคำตอบเดียว)</label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div><button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ตัวเลือก</label>
                                                        <div class="col-lg-6">
                                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-success" >เพิ่มตัวเลือก </button>
                                                        <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />อื่น ๆ ระบุ  <i class="far fa-square"></i> <button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />อื่น ๆ ระบุ  <i class="far fa-square"></i> <button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        </div> 
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">คำถาม (แบบหลายคำตอบ)</label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div><button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ตัวเลือก</label>
                                                        <div class="col-lg-6">
                                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-success" >เพิ่มตัวเลือก </button>
                                                        <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />อื่น ๆ ระบุ  <i class="far fa-square"></i> <button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />อื่น ๆ ระบุ  <i class="far fa-square"></i> <button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        </div> 
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">กลุ่ม (แบบให้คะแนน)</label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div><button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                        <label class="col-sm-3 control-label text-sm-right pt-2"></label>
                                                        <div class="col-lg-6">
                                                            ระดับคะแนน <br />
                                                            <i class="far fa-square"></i> 5 คะแนน<br />
                                                            <i class="far fa-square"></i> 10 คะแนน
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">คำถาม (แบบตอบบรรทัดเดียว)</label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div><button type="button" class="mb-1 mt-1 mr-1 btn btn-info" >ลบ </button>
                                                    </div>

                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>