<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบข่าวประชาสัมพันธ์</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการข่าวประชาสัมพันธ์</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            
                            <li><span>ระบบข่าวประชาสัมพันธ์</span></li>
                            <li><span>จัดการข่าวประชาสัมพันธ์</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>


            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>