  <header class="header">
      <div class="logo-container">
          <a href="../3.1.0" class="logo">
              <img src="img/logo.png" height="50" alt="Deves Admin" />
          </a>
          <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
              <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
          </div>
      </div>

      <div class="header-right">

          <span class="separator"></span>

          <ul class="notifications">

              <li>
                  <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                      <i class="bx bx-book"></i>
                  </a>

                  <div class="dropdown-menu notification-menu">
                      <div class="notification-title">
                          คู่มือการใช้งาน
                      </div>

                      <div class="content">
                          <ul>
                              <li>
                                  <a href="#" class="clearfix">
                                      <div class="image">
                                          <i class="fas fa-book bg-danger text-light"></i>
                                      </div>
                                      <span class="title">Download คู่มือการใช้งาน</span>
                                      <span class="message">PDF file</span>
                                  </a>
                              </li>
                          </ul>
                      </div>
                  </div>
              </li>
          </ul>

          <span class="separator"></span>

          <div id="userbox" class="userbox">
              <a href="#" data-toggle="dropdown">
                  <figure class="profile-picture">
                      <img src="img/!logged-user.jpg" alt="Joseph Doe" class="rounded-circle" data-lock-picture="img/!logged-user.jpg" />
                  </figure>
                  <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                      <span class="name">John Doe Junior</span>
                      <span class="role">Administrator</span>
                  </div>

                  <i class="fa custom-caret"></i>
              </a>

              <div class="dropdown-menu">
                  <ul class="list-unstyled mb-2">
                      <li class="divider"></li>
                      <li>
                          <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="bx bx-lock"></i> Lock Screen</a>
                      </li>
                      <li>
                          <a role="menuitem" tabindex="-1" href="pages-signin.php"><i class="bx bx-power-off"></i> Logout</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </header>