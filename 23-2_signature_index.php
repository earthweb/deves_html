<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบประกาศนียบัตร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการลายเซนต์</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบประกาศนียบัตร</span></li>
                            <li><span>จัดการลายเซนต์</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-search"></i> ค้นหาขั้นสูง</h2>
                            </header>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อลายเซ็นต์</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                        <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-search"></i> ค้นหา</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-table"></i> จัดการลายเซนต์</h2>
                            </header>

                            <div class="card-body">
                                <a href="23-2-1_signature_index.php">
                                    <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-plus-circle"></i> เพิ่มลายเซนต์</button>
                                </a>

                                <table class="table table-bordered  mb-0" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th class="text-center" class="" width="10px"><i class="far fa-square"></i></th>
                                            <th class="">ชื่อลายเซ็นต์</th>
                                            <th class="">วันที่เพิ่มข้อมูล</th>
                                            <th class="">เปิด/ปิด การแสดงผล</th>
                                            <th class="">จัดการ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <i class="far fa-square">
                                            </td>
                                            <td>
                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                            </td>

                                            <td>
                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                            </td>
                                            <td>

                                            </td>

                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="far fa-square">
                                            </td>
                                            <td></td>

                                            <td></td>
                                            <td>
                                                <div class="form-group row">
                                                    <div class="col-lg-9">
                                                        <div class="switch switch-success">
                                                            <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <i class="far fa-square">
                                            </td>
                                            <td></td>

                                            <td></td>
                                            <td>
                                                <div class="form-group row">
                                                    <div class="col-lg-9">
                                                        <div class="switch switch-success">
                                                            <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>

                                <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-trash"></i> ลบข้อมูลทั้งหมด</button>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>