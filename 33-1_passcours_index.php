<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>พิมพใบประกาศนียบัตร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>ผู้ผ่านการเรียน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>พิมพใบประกาศนียบัตร</span></li>
                            <li><span>ผู้ผ่านการเรียน</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>




                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">ค้นหา</h2>
                                </header>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                                <div>
                                                    

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">เลือกรุ่น :</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup label="">
                                                                    <option value="AK">---กรุณาเลือกรุ่น---</option>
                                                                    <option value="HI">1</option>
                                                                    <option value="HI">2</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
												<label class="col-lg-3 control-label text-lg-right pt-2">เลือกหลักสูตร :</label>
												<div class="col-lg-6">
													<select multiple="" class="form-control">
														<option>1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
													</select>
												</div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ค้นหา :</label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            <span class="required">* (สามารถค้นหาด้วย ชื่อ, สกุล หรือบัตรประชาชน)</span>
                                                        </div>
                                                    </div>

                                            <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2" for="textareaDefault">วันที่เริ่มต้น :</label>
                                                        <div class="col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fas fa-calendar-alt"></i>
                                                                    </span>
                                                                </span>
                                                                <input type="text" data-plugin-datepicker class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2" for="textareaDefault">วันที่สิ้นสุด :</label>
                                                        <div class="col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fas fa-calendar-alt"></i>
                                                                    </span>
                                                                </span>
                                                                <input type="text" data-plugin-datepicker class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-search"></i> search</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-table"></i> ระบบReport ผู้ผ่านการเรียน</h2>
                            </header>
                            <div class="card-body">
                                <table class="table table-bordered  mb-0" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th class="">ชื่อ - สกุล</th>
                                            <th class="">รหัสบัตรประชาชน</th>
                                            <th class="">ประเภทสมาชิก</th>
                                            <th class="">สมาชิก	ที่อยู่</th>
                                            <th class="">จังหวัด</th>
                                            <th class="">ประเภทธุรกิจ</th>
                                            <th class="">โทรศัพท์</th>
                                            <th class="">e-mail</th>
                                            <th class="">ชื่อหลักสูตร</th>
                                            <th class="">วันที่สอบผ่าน</th>
                                            <th class="">พิมพ์ใบผ่านการอบรม</th>
                                            <th class="">บันทึกใบผ่านการประกาศนียบัตร</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                    </tbody>
                                </table>

                                <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-file-export"></i> Export</button>

                            </div>
                        </section>
                    </div>
                </div>

            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>