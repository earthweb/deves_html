<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ภาษา</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการภาษา</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ภาษา</span></li>
                            <li><span>จัดการภาษา</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>
            
                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-table"></i> จัดการภาษา</h2>
                            </header>
                            <div class="card-body">
                                <table class="table table-bordered  mb-0" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th class="">ภาษา</th>
                                            <th class="text-center"  width="90px">จัดการ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>TH</td>
                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>EN</td>
                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                       

                                    </tbody>
                                </table>

                                <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-trash"></i> ลบที่เลือก</button>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>