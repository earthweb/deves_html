<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบกำหนดสิทธิการใช้งาน</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่ม controller</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบกำหนดสิทธิการใช้งาน</span></li>
                            <li><span>เพิ่ม controller</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-table"></i> เพิ่ม controller</h2>
                            </header>
                            <div class="card-body">
                            <a href="29-3-1_pcontroller_index.php">
                                <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-plus-circle"></i> เพิ่ม controller</button>
                            </a>
                            
                                <table class="table table-bordered  mb-0" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th class="text-center" class="" width="10px"><i class="far fa-square"></i></th>
                                            <th class="">NO.</th>
                                            <th class="">Title</th>
                                            <th class="">Controller</th>
                                            <th class="">Active</th>
                                            <th class="">priority</th>
                                            <th class=""></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                            </td>
                                            <td>
                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>

                                        </tr>
                                        <tr>
                                            <td><i class="far fa-square"></i></td>

                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><i class="fas fa-plus"></i></td>
                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td><i class="far fa-square"></i></td>
                                            <td></td>
                                            <td>

                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td><i class="fas fa-plus"></i></td>
                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                            
                                        </tr>


                                    </tbody>
                                </table>

                                <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-trash"></i> ลบข้อมูลที่เลือก</button>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>