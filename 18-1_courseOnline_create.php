<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title><span class="badge badge-primary">182</span>หลักสูตร</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มหลักสูตร</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>หลักสูตร</span></li>
                            <li><span>เพิ่มหลักสูตร</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>




                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">เพิ่มหลักสูตร</h2>
                                </header>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                                <div>
                                                    <div class="alert alert-danger mt-2">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        ค่าที่มี <i class="fas fa-question-circle"></i> จำเป็นต้องใส่ให้ครบ
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">หมวดอบรมออนไลน์ <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup label="">
                                                                    <option value="AK">Alaska</option>
                                                                    <option value="HI">Hawaii</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อหลักสูตรอบรมออนไลน์ <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">รหัสหลักสูตร</label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2" for="textareaDefault">รายละเอียดย่อ <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <textarea class="form-control" rows="3" data-plugin-maxlength maxlength="140"></textarea>
                                                        </div>
                                                    </div>

                                                    <form class="form-horizontal form-bordered">
                                                        <div class="form-group row">
                                                            <label class="col-lg-3 control-label text-lg-right pt-2">รายละเอียดหลักสูตร</label>
                                                            <div class="col-lg-9">
                                                                <div class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }'></div>
                                                                
                                                          
                                                            </div>
                                                        </div>

                                                    </form>
                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2" for="textareaDefault">วันที่เริ่มต้นการเข้าเรียน</label>
                                                        <div class="col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fas fa-calendar-alt"></i>
                                                                    </span>
                                                                </span>
                                                                <input type="text" data-plugin-datepicker class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2">เวลาเริ่มต้นการเข้าเรียน</label>
                                                        <div class="col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="far fa-clock"></i>
                                                                    </span>
                                                                </span>
                                                                <input type="text" data-plugin-timepicker class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2" for="textareaDefault">วันที่สิ้นสุดการเข้าเรียน</label>
                                                        <div class="col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fas fa-calendar-alt"></i>
                                                                    </span>
                                                                </span>
                                                                <input type="text" data-plugin-datepicker class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2">เวลาสิ้นสุดการเข้าเรียน</label>
                                                        <div class="col-lg-6">
                                                            <div class="input-group">
                                                                <span class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="far fa-clock"></i>
                                                                    </span>
                                                                </span>
                                                                <input type="text" data-plugin-timepicker class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">จำนวนวันที่เข้าเรียน <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">เกณฑ์การสอบผ่าน (เปอร์เซ็น) <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">จำนวนครั้งที่ทำข้อสอบได้ (ครั้ง) <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">เวลาในการทำข้อสอบ (นาที) <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">ระดับหลักสูตร <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <select data-plugin-selectTwo class="form-control populate">
                                                                <optgroup label="">
                                                                    <option value="AK">ระดับพื้นฐาน</option>
                                                                    <option value="HI">ระดับกลาง</option>
                                                                    <option value="HI">ระดับสูง</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">คะแนนหลักสูตร</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2 col-lg-3">ปักหมุดหลักสูตรแนะนำ</label>
                                                        <div class="col-lg-9">
                                                            <div class="switch switch-success">
                                                                <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-lg-3 control-label text-lg-right pt-2 col-lg-3">หลักสูตรจ่ายเงิน</label>
                                                        <div class="col-lg-9">
                                                            <div class="switch switch-success">
                                                                <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                                    <label class="col-lg-3 control-label text-lg-right pt-2">รูปภาพ</span></label>
                                                                    <div class="col-lg-6">
                                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="input-append">
                                                                                <div class="uneditable-input">
                                                                                    <i class="fas fa-file fileupload-exists"></i>
                                                                                    <span class="fileupload-preview"></span>
                                                                                </div>
                                                                                <span class="btn btn-default btn-file">
                                                                                    <span class="fileupload-exists">เปลืยน</span>
                                                                                    <span class="fileupload-new">เลือกไฟล์</span>
                                                                                    <input type="file" />
                                                                                </span>
                                                                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">ลบ</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="alert alert-danger mt-2">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                                            <i class="fas fa-question-circle"></i>  รูปภาพควรมีขนาด 250x180(แนวนอน) หรือ ขนาด 250x(xxx) (แนวยาว)
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>