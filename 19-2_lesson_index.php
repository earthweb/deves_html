<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบบทเรียน</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>

    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>จัดการบทเรียน</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบบทเรียน</span></li>
                            <li><span>จัดการบทเรียน</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-search"></i> ค้นหาขั้นสูง</h2>
                            </header>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label text-sm-right pt-2">หลักสูตรอบรมออนไลน์</label>
                                    <div class="col-lg-6">
                                        <select data-plugin-selectTwo class="form-control populate">
                                            <optgroup label="">
                                                <option value="AK">---กรุณาเลือกหลักสูตร---</option>
                                                <option value="AK">บทที่1</option>
                                                <option value="HI">บทที่2</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อบทเรียน</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-search"></i> ค้นหา</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <section class="card mb-4">
                            <header class="card-header">
                                <div class="card-actions">
                                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                </div>

                                <h2 class="card-title"><i class="fas fa-table"></i> จัดการบทเรียน</h2>
                            </header>
                            <div class="card-body">
                                <table class="table table-bordered  mb-0" id="datatable-default">
                                    <thead>
                                        <tr>
                                            <th class="text-center" class="" width="10px"><i class="far fa-square"></i></th>
                                            <th class="">หลักสูตรอบรมออนไลน์</th>
                                            <th class="text-center" width="90px">ชื่อบทเรียน</th>
                                            <th class="text-center" width="90px">จัดการวิดีโอ</th>
                                            <th class="text-center" width="90px">ก่อนเรียน</th>
                                            <th class="text-center" width="90px">หลังเรียน</th>
                                            <th class="text-center" width="90px">ภาษา</th>
                                            <th class="text-center" width="90px">จัดการ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><i class="far fa-square"></i></td>
                                            <td>
                                                <div class="form-group row">

                                                    <div class="col-lg-6">
                                                        <select data-plugin-selectTwo class="form-control populate">
                                                            <optgroup label="">
                                                                <option value="AK">---กรุณาเลือกหลักสูตร---</option>
                                                                <option value="AK">บทที่1</option>
                                                                <option value="HI">บทที่2</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>

                                        </tr>
                                        <tr>
                                            <td><i class="far fa-square"></i></td>
                                            <td></td>
                                            <td>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">(จัดการVDO)</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">(เลือกข้อสอบ)</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">(เลือกข้อสอบ)</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">EN (แก้ไข)</button>
                                                    <button type="button" class="btn btn-primary">TH (แก้ไข)</button>
                                                </div>
                                            </td>
                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><i class="far fa-square"></i></td>
                                            <td></td>
                                            <td>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">(จัดการVDO)</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">(เลือกข้อสอบ)</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">(เลือกข้อสอบ)</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn-group-vertical d-block ">
                                                    <button type="button" class="btn btn-primary">EN (แก้ไข)</button>
                                                    <button type="button" class="btn btn-primary">TH (แก้ไข)</button>
                                                </div>
                                            </td>
                                            <td class="actions text-center">
                                                <a href=""><i class="fas fa-pencil-alt"></i></a>
                                                <a href="" class="delete-row"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>

                                <button id="remove-row" type="button" class="mb-1 mt-1 mr-1 btn btn-primary"><i class="fas fa-trash"></i> ลบข้อมูลทั้งหมด</button>

                            </div>
                        </section>
                    </div>
                </div>

            </section>


        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>