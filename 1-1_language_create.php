<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>เพิ่มภาษา</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>
        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่มภาษา</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.php">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ภาษา</span></li>
                            <li><span>เพิ่มภาษา</span></li>
                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>

                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">เพิ่มภาษา</h2>
                                </header>
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label class="col-sm-3 control-label text-sm-right pt-2">ชื่อภาษา <span class="required">*</span></label>
                                        <div class="col-lg-6">
                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2">รูปภาพธงชาติ</label>
                                        <div class="col-lg-6">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="input-append">
                                                    <div class="uneditable-input">
                                                        <i class="fas fa-file fileupload-exists"></i>
                                                        <span class="fileupload-preview"></span>
                                                    </div>
                                                    <span class="btn btn-default btn-file">
                                                        <span class="fileupload-exists">เปลืยน</span>
                                                        <span class="fileupload-new">เลือกไฟล์</span>
                                                        <input type="file" />
                                                    </span>
                                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">ลบ</a>
                                                </div>
                                            </div>
                                            <div class="alert alert-danger mt-2">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <i class="fas fa-question-circle"></i> รูปภาพควรมีขนาด <strong>30 x 30 px</strong>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 control-label text-lg-right pt-2 col-lg-3">สถานะแสดงผล</label>
                                        <div class="col-lg-9">
                                            <div class="switch switch-success">
                                                <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> บันทึกข้อมูล</button>
                                            <button type="reset" class="btn btn-default">รีเซ็ต</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>

                        <!-- <div class="col-lg-6">
                            <form id="summary-form" action="forms-validation.html" class="form-horizontal">
                                <section class="card">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                        </div>

                                        <h2 class="card-title">Validation Summary</h2>
                                        <p class="card-subtitle">
                                            Validation summary will display an error list above the form.
                                        </p>
                                    </header>
                                    <div class="card-body">
                                        <div class="validation-message">
                                            <ul></ul>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Full Name <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="fullname" class="form-control" title="Please enter a name." placeholder="eg.: John Doe" required />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Email <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="email" name="email" class="form-control" title="Please enter an email address." placeholder="eg.: john@doe.com" required />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">GitHub</label>
                                            <div class="col-sm-9">
                                                <input type="url" name="url" title="Please enter a valid url." class="form-control" placeholder="eg.: https://github.com/johndoe" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Resume <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <textarea name="resume" rows="5" title="Your resume is too short." class="form-control" placeholder="Enter your resume" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row justify-content-end">
                                            <div class="col-sm-9">
                                                <button class="btn btn-primary">Submit</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </form>
                        </div> -->
                    </div>
                </div>

        </div>

    </section>
    </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>