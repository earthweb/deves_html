<!doctype html>
<html class="fixed">

<head>
    <meta charset="UTF-8">
    <title>ระบบกำหนดสิทธิการใช้งาน</title>
    <?php include 'include/inc-head.php'; ?>
</head>

<body>
    <section class="body">
        <?php include 'include/inc-header.php'; ?>

        <div class="inner-wrapper">
            <?php include 'include/inc-menuleft.php'; ?>
            <?php include 'include/inc-menuright.php'; ?>

            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>เพิ่ม controller</h2>

                    <div class="right-wrapper text-right">
                        <ol class="breadcrumbs">
                            <li>
                                <a href="index.html">
                                    <i class="bx bx-home-alt"></i>
                                </a>
                            </li>
                            <li><span>ระบบกำหนดสิทธิการใช้งาน</span></li>
                            <li><span>เพิ่ม controller</span></li>
                            <li><span>จัดการ controller</span></li>

                        </ol>

                        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
                    </div>
                </header>




                <div class="row">
                    <div class="col">
                        <form id="form" action="" class="form-horizontal">
                            <section class="card">
                                <header class="card-header">
                                    <div class="card-actions">
                                        <a href="#" class="card-actiโon card-action-toggle" data-card-toggle></a>
                                    </div>

                                    <h2 class="card-title">จัดการ controller</h2>
                                </header>
                                <div class="row">
                                    <div class="col">
                                        <section class="card">

                                            <div class="card-body">
                                            Fields with * are required.
                                                <div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-3 control-label text-sm-right pt-2">Title <span class="required">*</span></label>
                                                        <div class="col-lg-6">
                                                            <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                        </div>
                                                    </div>

                                                    <div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-3 control-label text-sm-right pt-2">Controller <span class="required">*</span></label>
                                                            <div class="col-lg-6">
                                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row">
                                                         
                                                            <label class="col-sm-3 control-label text-sm-right pt-2">Title </label>
                                                            <div class="col-lg-6">
                                                            Title <span class="required">*</span>
                                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                                Action <span class="required">*</span>
                                                                <input class="form-control" placeholder="" data-plugin-maxlength maxlength="20" required />
                                                                <button class="btn btn-primary"><i class="fas fa-plus"></i></button><br />
                                                                Press <i class="fas fa-plus"></i>  to add another form field :)

                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <footer class="card-footer">
                                    <div class="row justify-content-end">
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary"><i class="fas fa-check"></i> create</button>
                                        </div>
                                    </div>
                                </footer>
                            </section>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </section>
    <?php include 'include/inc-script.php'; ?>
</body>

</html>